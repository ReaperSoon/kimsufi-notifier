"use strict";

module.exports = function(sequelize, DataTypes) {
  var Subscriber = sequelize.define('Subscriber', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        unique: true,
        allowNull: false
    },
    type: {
      type: DataTypes.ENUM,
      values: ['email', 'phone']
    },
    contact: DataTypes.STRING,
    server_id: DataTypes.INTEGER,
    notified: DataTypes.INTEGER,
    last_notified: DataTypes.DATE,
    deleted: DataTypes.BOOLEAN
  },
  {
    timestamps: false,
    paranoid: false,
    underscored: true,
    freezeTableName: true,
    tableName: 'subscriber',
    classMethods: {
      associate: function(models) {
        Subscriber.belongsTo(models.Server, { foreignKey: 'server_id' });
      }
    }
  });

  return Subscriber;
};