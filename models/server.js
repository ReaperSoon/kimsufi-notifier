"use strict";

module.exports = function(sequelize, DataTypes) {
  var Server = sequelize.define('Server', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        unique: true,
        allowNull: false
    },
    reference: DataTypes.STRING,
    name: DataTypes.STRING,
    subscribed: DataTypes.INTEGER,
    email_sent: DataTypes.INTEGER,
    sms_sent: DataTypes.INTEGER
  },
  {
    timestamps: false,
    paranoid: false,
    underscored: true,
    freezeTableName: true,
    tableName: 'server'
  });

  return Server;
};