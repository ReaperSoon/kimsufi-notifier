"use strict";

var express = require('express');
var path = require('path');
var morgan = require('morgan');
var fs = require('fs');
var readline = require('readline');

var RoutesManager = require('./modules/routes_manager.js');
var KimsufiManager = require('./modules/kimsufi_manager.js');
var config = require('./modules/config.js');
var db = require('./modules/database_manager.js');

// Anti crash
process.on('uncaughtException', function (err) {
    console.log('Caught exception: ' + err);
    console.log(err.stack);
    if (config.ENABLE_CHECK === true) {
		console.log("Kimsufi Manager a été relancé.");
		KimsufiManager.checkAvailability();
	}
});

/***********************************
 *** Check Availability Mecanism ***
 ***********************************/

if (config.ENABLE_CHECK === true) {
	if (config.FAKE_MODE === true)
		console.log('[WARNING] FAKE MODE ENABLED');
	console.log('Checking availability...');
	KimsufiManager.checkAvailability();
}

/***********************
 *** Express Context ***
 ***********************/

var app = express();
/* Setup front */
app.set('view engine','twig');
app.set("twig options", { strict_variables: false });
app.set('views', __dirname+'/views');
app.set('view engine', 'twig');
app.use(express.static(path.join(__dirname, 'public')));
/* Some middleware to parse the post data of the body */
app.use(express.json());
app.use(express.urlencoded());
/* Create access log file */
var accessLogStream = fs.createWriteStream(__dirname + '/logs/access.log', {flags: 'a'});
app.use(morgan('combined', {stream: accessLogStream}));

if (app.get('env') == 'developpement') {
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
  config.SITE_URL = config.DEV_URL + ':' + process.env.PORT || 5000;
  config.ENV = app.get('env');
  
}else if (app.get('env') == 'production') {
  app.use(express.errorHandler());
  config.SITE_URL = config.PROD_URL;
  config.ENV = app.get('env');
}else {
	console.log('No valid env found, using default : developpement');
	app.set('env', 'developpement');
	app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
	config.SITE_URL = config.DEV_URL + ':' + (process.env.PORT || 5000);
	config.ENV = app.get('env');
}

RoutesManager.setupRoutes(app);

app.listen(process.env.PORT || 5000);
console.log('Kimsufi-notifier is now running in ' + config.ENV + ' mode at: ' + config.SITE_URL);

/***********************
 ***  Prompt System  ***
 ***********************/

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

rl.setPrompt('KSN> ');
rl.prompt();

rl.on('line', function(line) {
    switch(line.trim()) {
        case 'show-subscribers':
        	db.getAllSubscribers(function(subscribers) {
        		console.log(JSON.stringify(subscribers, null, 2));
        	});
            break;
        case 'show-deleted-subscribers':
        	db.getAllSubscribers(function(subscribers) {
        		console.log(JSON.stringify(subscribers, null, 2));
        	}, true);
            break;
        case 'show-alive-subscribers':
        	db.getAllSubscribers(function(subscribers) {
        		console.log(JSON.stringify(subscribers, null, 2));
        	}, false);
            break;
        case 'lastcheck':
        	console.log(new Date(KimsufiManager.getLastCheck()));
        	break;
        case 'reset':
        	var keys = Object.keys(config.SERVERS.subject);
			keys.forEach(function(key) {
				config.SERVERS.get(key).set('registered_emails', []);
				config.SERVERS.get(key).set('registered_phones', []);
			});
    		console.log("Reset");
        	break;
        case 'exit':
        	process.exit(0);
        	break;
        default:
            console.log('Invalid command `' + line.trim() + '`');
        break;
    }
    rl.prompt();
}).on('close', function() {
    console.log('Have a great day!');
    process.exit(0);
});