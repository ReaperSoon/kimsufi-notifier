"use strict";

var config = require('./config.js');

function Subscriber (contact) {
    this.contact = contact;
    this.lastNotified = 0;
    this.notifiedCount = 0;
}

Subscriber.prototype.readyToBeNotified = function() {
    return (Date.now() - this.lastNotified) >= config.NOTIFY_DELAY;
};

Subscriber.prototype.updateLastNotified = function() {
    this.lastNotified = Date.now();
};

Subscriber.prototype.fromJson = function(json) {
    this.contact = json.contact;
    this.lastNotified = json.lastNotified;
    this.notifiedCount = json.notifiedCount;

    return this;
};

module.exports = Subscriber;