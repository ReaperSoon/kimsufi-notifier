"use strict";

var utils = require('./utils.js');
var config = require('./config.js');
var db = require('./database_manager.js');

var setupRoutes = function(app) {
	homeRoute(app);
	subscribeRoute(app);
	unsubscribeRoute(app);
	legalRoute(app);
};

function homeRoute(app) {
	app.get('/', function(req, res) {
		db.getAllServers(function(servers) {
			res.render('full/home.twig', {
				servers: servers
			});
		});
	});
}

function subscribeRoute(app) {
	app.post('/subscribe', function(req, res) {
       	var email = req.body.email;
       	var phone = req.body.phone;
       	var ref = req.body.server;

       	db.getServerByRef(ref, function(server) {
       		if (server === null) {
       			res.redirect('/');
       		}else {
       			if (email !== '') {
       				res.redirect('/subscribe/email/' + email + '/' + ref);
		       	}else if (phone !== '') {
		       		res.redirect('/subscribe/phone/' + phone + '/' + ref);
		       	}else {
		   			res.redirect('/');
		       	}
       		}
       	});
	});

	app.get('/subscribe/:type/:contact/:ref', function(req, res) {
		var type = req.params.type;
		var contact = req.params.contact;
		var ref = req.params.ref;

		var render = function(result) {
			res.render('full/subscribed.twig', {
	       		type: type,
	       		result : result
			});
		};

		if ((type === 'email' && utils.validateEmail(contact) === false) || (type === 'phone' && utils.validatePhone(contact) === false)) {
			var type_fr = type === 'phone' ? 'téléphone' : type;
			render({
				error:  'Votre ' + type_fr + ' ' + contact + ' est invalide.<br>' +
						'Merci d\'utiliser un ' + type_fr + ' valide, si le problème persiste merci de <a href="mailto:' + config.ADMIN_EMAIL + '">contacter l\'administrateur</a>'
			});
		}else {
			db.addSubscriberByRef(ref, contact, type, render);
		}
	});
}

function unsubscribeRoute(app) {
	app.get('/unsubscribe', function(req, res) {
		db.getAllServers(function(servers) {
			res.render('full/unsubscribe.twig', {
				servers: servers
			});
		});
	});

	app.post('/unsubscribe', function(req, res) {
       	var email = req.body.email;
       	var phone = req.body.phone;
       	var ref = req.body.server;

       	if (utils.validateEmail(email) === true) {
       		res.redirect('/unsubscribe/email/' + email + '/' + ref);
       	}else if (utils.validatePhone(phone) === true) {
       		res.redirect('/unsubscribe/phone/' + phone + '/' + ref);
       	}else {
       		res.redirect('/');
       	}
	});

	app.get('/unsubscribe/:type/:contact/:ref?', function(req, res) {
		var type = req.params.type;
		var contact = req.params.contact;
		var ref = req.params.ref;

		var render = function(result) {
			console.log(result);
			res.render('full/unsubscribed.twig', {
	       		type: type,
	       		contact: contact,
	       		result : result
			});
		};

		if ((type === 'email' && utils.validateEmail(contact) === false) || (type === 'phone' && utils.validatePhone(contact) === false)) {
			var type_fr = type === 'phone' ? 'téléphone' : type;
			render({
				error:  'Votre ' + type_fr + ' ' + contact + ' est invalide.<br>' +
						'Merci d\'utiliser un ' + type_fr + ' valide, si le problème persiste merci de <a href="mailto:' + config.ADMIN_EMAIL + '">contacter l\'administrateur</a>'
			});
		}else if (ref && typeof ref !== 'undefined' && ref !== null) {
			db.removeSubscriberByRef(ref, contact, render);
		}else {
			db.removeSubscriberFromAll(contact, render);
		}

	});
}

function legalRoute(app) {
	app.get('/legal', function(req, res) {
		res.render('full/legal.twig');
	});
}

exports.setupRoutes = setupRoutes;