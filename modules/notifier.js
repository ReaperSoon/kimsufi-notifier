"use strict";

var config = require('./config.js');
var network = require('./network_manager.js');
var db = require('./database_manager.js');
var Subscriber = require('./subscriber.class.js');
var utils = require('./utils.js');

var notifyByEmail = function(subscribers, msg) {
	var sent = 0;
	var callback = function() {
		subscriber.updateLastNotified();
	};
	for (var i = 0; i < subscribers.length; i++) {
		var subscriber = new Subscriber().fromJson(subscribers[i]);
		var email = subscriber.contact;

		var message = msg.replaceAll('<email>', email);

		if (subscriber.readyToBeNotified()) {
			sent++;
			network.sendEmail(email, message, callback);
		}
	}
	if (sent > 0)
		console.log(sent + '/' + subscribers.length + ' emails ont été envoyés');
};

var notifyByEmailTemplated = function(subscriber, server) {
	function updateSubscriber(subscriber, server) {
		return function() {
			subscriber.last_notified = Date.now();
			subscriber.notified = subscriber.notified + 1;
			db.updateSubscriber(subscriber);
			if (subscriber.notified >= config.NOTIFY_LIMIT) {
				db.removeSubscriberByRef(server.reference, subscriber.contact, function(result) {
					console.log(result.subscribers[0].contact + ' removed from server ' + server.name);
				});
			}
			db.incEmailSentToServer(server);
		};
	}
	var email = subscriber.contact;
	var vars = {
		'server_name' : server.name,
		'unsubscribe' : config.SITE_URL + config.UNSUBSCRIBE_URI + '/email/' + email + '/' + server.reference,
		'url' : config.KIMSUFI_COMMAND_URL + server.reference
	};

	if (utils.readyToBeNotified(subscriber)) {
		network.sendTemplatedEmail(email, config.MAILER_TEMPLATE, vars, updateSubscriber(subscriber, server));
	}else {
		//console.log(subscriber.contact + ' not ready to be notified');
	}
};

var notifyBySms = function(subscriber, available_zones, server) {
	function updateSubscriber(subscriber, server) {
		return function(response) {
			/* if SMS successfully sent */
			if (response.error === 0) {
				subscriber.last_notified = Date.now();
				subscriber.notified = subscriber.notified + 1;
				db.updateSubscriber(subscriber);
				if (subscriber.notified >= config.NOTIFY_LIMIT) {
					db.removeSubscriberByRef(server.reference, subscriber.contact, function(result) {
						console.log(result.subscribers[0].contact + ' removed from server ' + server.name);
					});
				}
				db.incSmsSentToServer(server);
			}
		};
	}

	var phone = subscriber.contact;

	var msg = 'Votre serveur Kimsufi ' + server.name + ' est disponible !\n';
	var plur = available_zones.length > 1 ? 's' : '';
    msg += 'Il est disponible dans ' + available_zones.length + ' datacenter' + plur + ' :\n';

    for (var z = 0; z < available_zones.length; z++) {
    	var zone_name = available_zones[z];
    	msg += '- ' + zone_name + '\n';
    }
    msg += 'Commandez le directement : ' + config.KIMSUFI_COMMAND_URL + server.reference + '\n\n';
    msg += 'Pour vous désinscrire : ' + config.SITE_URL + config.UNSUBSCRIBE_URI + '/phone/' + phone + '/' + server.reference;

	if (utils.readyToBeNotified(subscriber)) {
		network.sendSMS(phone, msg, updateSubscriber(subscriber, server));
	}else {
		//console.log(subscriber.contact + ' not ready to be notified');
	}
};

var notifySubscribers = function(subscribers, server, zones) {
	for (var i = subscribers.length - 1; i >= 0; i--) {
		var subscriber = subscribers[i];
		if (subscriber.type === 'email') {
			notifyByEmailTemplated(subscriber, server);
		}else if (subscriber.type === 'phone') {
			notifyBySms(subscriber, zones, server);
		}
	}
};

exports.notifyByEmail = notifyByEmail;
exports.notifyByEmailTemplated = notifyByEmailTemplated;
exports.notifyBySms = notifyBySms;
exports.notifySubscribers = notifySubscribers;
