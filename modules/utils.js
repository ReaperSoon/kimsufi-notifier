"use strict";

var config = require('./config.js');

var formatElapsedTime = function(time) {
	var result = [];
    var ss = Math.round(time % 60);
    time = Math.floor(time / 60);
    var mm = Math.round(time % 60);
    time = Math.floor(time / 60);
    var hh = Math.round(time % 24);
    time = Math.floor(time / 24);
    var dd = time ;

    var days = dd === 0 ? '' : (dd + ' jour');
    days = dd > 1 ? (days + 's') : days;
    if (days !== '') result.push(days);

    var hours = hh === 0 ? '' : (hh + ' heure');
    hours = hh > 1 ? (hours + 's') : hours;
    if (hours !== '') result.push(hours);

    var minuts = mm === 0 ? '' : (mm + ' minute');
    minuts = mm > 1 ? (minuts + 's') : minuts;
    if (minuts !== '') result.push(minuts);

    var seconds = ss === 0 ? '' : (ss + ' seconde');
    seconds = ss > 1 ? (seconds + 's') : seconds;
    if (seconds !== '') result.push(seconds);

    var formatedTime = result.join(' ');
    if (formatedTime === '')
    	return 'en ce moment!';
    else
    	return 'il y a ' + formatedTime;
};

var validateEmail = function(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};

var validatePhone = function(phone) {
    return ((phone.indexOf('06') === 0 || phone.indexOf('07') === 0) && phone.length === 10);
};

var validateJson = function(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
};

var readyToBeNotified = function(subscriber) {
    return (Date.now() - new Date(subscriber.last_notified) >= config.NOTIFY_DELAY);
};

function twoDigits(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

Date.prototype.toMysqlFormat = function() {
    return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds());
};

var joinObjectEncoded = function(object, glue, separator) {
    if (glue === undefined) {
        glue = '=';
    }

    if (separator === undefined) {
        separator = ',';
    }

    return Object.getOwnPropertyNames(object).map(function(k) {
        return [encodeURIComponent(k), encodeURIComponent(object[k])].join(glue);
    }).join(separator);
};

/*Object.prototype.getKeyByValue = function( value ) {
    for( var prop in this ) {
        if( this.hasOwnProperty( prop ) ) {
            if( this[ prop ] === value )
                return prop;
        }
    }
};*/

var findInArray = function(array, key, value) {
    for (var i = 0; i < array.length; i++) {
        if (array[i][key] === value) {
            return array[i];
        }
    }
    return null;
};

var findIndexInArray = function(array, key, value) {
    for (var i = 0; i < array.length; i++) {
        if (array[i][key] === value) {
            return i;
        }
    }
    return -1;
};

exports.joinObjectEncoded = joinObjectEncoded;
exports.readyToBeNotified = readyToBeNotified;
exports.formatElapsedTime = formatElapsedTime;
exports.validateEmail = validateEmail;
exports.validatePhone = validatePhone;
exports.validateJson = validateJson;
exports.findInArray = findInArray;
exports.findIndexInArray = findIndexInArray;
