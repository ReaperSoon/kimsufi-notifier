"use strict";

exports.ENV = 'undefined';
exports.ENABLE_CHECK = true;
exports.EXPRESS_VERBOSE = true;
exports.FAKE_MODE = false;

/* URLS */
exports.SITE_URL = 'undefined';
exports.ADMIN_EMAIL = 'contact@stevecohen.fr';

exports.FREE_USER = '29363549';
exports.FREE_TOKEN = 'ap9Y4dBWDSH8ht';
exports.RASPISMS_LOGIN = 'admin@example.fr';
exports.RASPISMS_PASSWORD = 'admin';
exports.RASPISMS_URL = 'http://172.20.10.6/RaspiSMS/smsAPI';

exports.MAILER_TOKEN = 'm4X2UMm5eQrHwHMEtdBPYBA5Cp9CWxphuJkX7A7yxP5muBq3RShLtBqj2aFDWNwH';
exports.MAILER_SUBJECT = 'Votre serveur kimsufi est disponible!';
exports.MAILER_SENDER = 'kimsufi-notifier';
exports.MAILER_TEMPLATE = 'kimsufi-notifier';

exports.PROD_URL = 'http://kimsufi-notifier.heroku.com';
exports.DEV_URL = 'http://127.0.0.1';
exports.KIMSUFI_COMMAND_URL = 'https://www.kimsufi.com/fr/commande/kimsufi.xml?reference=';
exports.UNSUBSCRIBE_URI = '/unsubscribe';

exports.SUBSCRIBED_FILE = 'subscribed.json';

exports.OVH_FAKE_DATA = 'data/availability.ovh.json';
exports.OVH_CHECK_URL = 'https://ws.ovh.com/dedicated/r2/ws.dispatcher/getAvailability2';
exports.OVH_LASTTIME_TAKEN = 'https://ws.ovh.com/dedicated/r2/ws.dispatcher/getElapsedTimeSinceLastDelivery?params=';

exports.NOTIFY_LIMIT = 10;
exports.NOTIFY_DELAY = 1*24*60*60*1000; // 1j in ms
exports.CHECKTIME = 10*1000; // 10 seconds in ms
exports.CHECKTIME_FAKE_MODE = 1*1000; // 3 seconds in ms

exports.SERVERS = {
    "160sk1": {
        name: "KS-1"
    },
    "160sk2": {
        name: "KS-2A"
    },
    "161sk2": {
        name: "KS-2E"
    },
    "160sk3": {
        name: "KS-3A"
    },
    "160sk31": {
        name: "KS-3B"
    },
    "160sk4": {
        name: "KS-4A"
    },
    "160sk5": {
        name: "KS-5"
    },
    "160sk21": {
        name: "KS-2B"
    },
    "160sk23": {
        name: "KS-2D"
    },
    "160sk32": {
        name: "KS-3C"
    },
    "160sk42": {
        name: "KS-4C"
    }
};

exports.ZONES = {
    'gra': 'Gravelines',
    'sbg': 'Strasbourg',
    'rbx': 'Roubaix',
    'rbx-hz': 'Roubaix HZ',
    'bhs': 'Beauharnois'
};