"use strict";

var request = require("request");
var config = require('./config.js');
var utils = require('./utils.js');

function req(url, type, data, callback) {
    var obj = {
        url: url,
        method: type,
        timeout: 30000
    };

    if (data !== null && typeof data != 'undefined') {
        obj.json = true;
        obj.headers = { "content-type": "application/json" };
        obj.body = JSON.stringify(data);
    }

    request(obj, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            if (callback !== null && typeof callback != 'undefined')
                callback(body);
        }else {
            console.log("Request error: ", error);
            callback(body);
        }
    });
}

var sendSMS = function(phone, msg, callback) {
    // OLD => 'https://smsapi.free-mobile.fr/sendmsg?user=' + config.FREE_USER + '&pass=' + config.FREE_TOKEN + '&msg=' + encodeURIComponent(msg),
    // NEW => http://raspisms.stevecohen.fr/smsAPI/?email=cohensteve@hotmail.fr&password=qRkkzifB01041992!&numbers=0612345678\&text=Texte%20du%20SMS%20%C3%A0%20envoyer
    req(
        config.RASPISMS_URL + '/?email=' + config.RASPISMS_LOGIN + '&password=' + config.RASPISMS_PASSWORD + '&numbers=' + phone + '&text=' + encodeURIComponent(msg),
        'GET',
        null,
        function(data) {
            var response = JSON.parse(data);
            switch(response.error) {
                case 0:
                    console.log('RASPISMS : Le SMS a été créé avec succès pour ' + phone);
                    break;
                case 1:
                    console.error('RASPISMS Erreur : Les identifiants fournis sont incorrects');
                    break;
                case 2:
                    console.error('RASPISMS Erreur : La création du SMS a échoué');
                    break;
                case 3:
                    console.error('RASPISMS Erreur : Des arguments obligatoires sont manquants');
                    break;
                default:
                    console.error('RASPISMS Erreur : Erreur inconnue');
            }
            if (typeof callback !== 'undefined') {
                callback(response);
            }
        }
    );
};

var sendEmail = function(address, msg, callback) {
    console.log('Sending email to ' + address + ' : ' + msg);
    req(
        'http://mailer.stevecohen.fr/api/send?token=' + encodeURIComponent(config.MAILER_TOKEN) + '&to=' + encodeURIComponent(address) + '&subject=' + encodeURIComponent(config.MAILER_SUBJECT) + '&body=' + encodeURIComponent(msg),
        'GET',
        null,
        function(response) {
            console.log(response);
            if (typeof callback !== 'undefined') {
                callback(response);
            }
        }
    );
};

var sendTemplatedEmail = function(address, template, vars, callback) {
    console.log('Sending email to ' + address);
    if (config.FAKE_MODE) {
        callback();
        return ;
    }
    var url = 'http://mailer.stevecohen.fr/api/sendhtml?' +
            'token=' + encodeURIComponent(config.MAILER_TOKEN) +
            '&from=' + encodeURIComponent(config.MAILER_SENDER) + 
            '&to=' + encodeURIComponent(address) + 
            '&subject=' + encodeURIComponent(config.MAILER_SUBJECT) + 
            '&template=' + encodeURIComponent(config.MAILER_TEMPLATE) + 
            '&vars=' + utils.joinObjectEncoded(vars, ':', ';');
    console.log(url);
    req(
       //http://mailer.stevecohen.fr/api/sendhtml?token=m4X2UMm5eQrHwHMEtdBPYBA5Cp9CWxphuJkX7A7yxP5muBq3RShLtBqj2aFDWNwH&from=kimsufi-notifier&to=steve.cohen1134%40gmail.com&subject=Your%20kimsufi%20is%20available!&template=kimsufi-notifier&vars=ref:KS-2A
        url,
        'GET',
        null,
        function(response) {
            console.log("Response : ", response);
            if (typeof callback !== 'undefined') {
                callback(response);
            }
        }
    );
};

exports.req = req;
exports.sendSMS = sendSMS;
exports.sendEmail = sendEmail;
exports.sendTemplatedEmail = sendTemplatedEmail;

