"use strict";

var fs = require('fs');

var network = require('./network_manager.js');
var config = require('./config.js');
var notifier = require('./notifier.js');
var utils = require('./utils.js');
var db = require('./database_manager.js');

var lastCheck = Date.now();

var checkAvailability = function() {
	lastCheck = Date.now();
	if (config.FAKE_MODE) {
		setTimeout(function() {
			console.log("Received FAKE DATA");
			getFakeOvhData(onResult);
		}, config.CHECKTIME_FAKE_MODE);
	}else {
	    network.req(config.OVH_CHECK_URL, 'GET', null, function(output) {
	    	if (output) {
	    		onResult(output);
	    	}else {
	    		console.log('No data received, check again in ' + Math.round(config.CHECKTIME/1000) + ' second(s)');
	    		setTimeout(checkAvailability, config.CHECKTIME);
	    	}
	    });
	}
};

var getFakeOvhData = function(callback) {
    fs.readFile(config.OVH_FAKE_DATA, 'utf-8', function(err, data) {
        if (err) {
            console.log(err);
            return ;
        }
        /* If invalide file */
        if (data === '' || !utils.validateJson(data)) {
            console.log('Invalide JSON Object from ' + config.OVH_FAKE_DATA);
            return ;
        }
        callback(data);
    });
};

var checklasttaken = function(ref) {
	network.req(config.OVH_LASTTIME_TAKEN + encodeURIComponent('{"gamme":"' + ref + '"}'), 'GET', null, function(output) {
		var obj = JSON.parse(output);

		if (obj.answer < config.LASTTAKENTIME) {
		    var takendate = new Date(Date.now() - (obj.answer * 1000)); // jshint ignore:line
		}
    });
};

var getLastCheck = function() {
	return lastCheck;
};

function onResult(output) {
	var checkSubscribers = function(server, available_zones) {
		return function(subscribers) {
			console.log('Server available : ' + config.SERVERS[server.reference].name);
	    	if (subscribers.length > 0) {
		    	console.log(subscribers.length + ' client have subscribed to this server');
		    	db.getServerByRef(server.reference, notifySubscribers(subscribers, available_zones));
		    }
		};
	};

	var notifySubscribers = function(subscribers, available_zones) {
		return function(dbserver) {
			notifier.notifySubscribers(subscribers, dbserver, available_zones);
		};
	};

    try {
    	console.log('--------------------------------------');
		var obj = JSON.parse(output);
		if (obj.answer === null) {
			console.log('Kimsufi service unavailable: ' + obj.error.status + ' - ' + obj.error.message);
		}else {
			var availability = obj.answer.availability;
			var available_zones = [];
			for (var i = 0; i < availability.length; i++) {
			    var server = availability[i];
			    if (server.reference in config.SERVERS) {
			    	available_zones = [];
					for (var y = 0; y < server.zones.length; y++) {
					    var zone = server.zones[y];
					    if (zone.availability != 'unknown' && zone.availability != 'unavailable' && zone.zone in config.ZONES) {
					    	available_zones.push(config.ZONES[zone.zone]);
					    }
					}
					if (available_zones.length > 0) {
					    db.getSubscribersByRef(server.reference, checkSubscribers(server, available_zones));
					}
			    }
			}
		}
		setTimeout(function() {
            checkAvailability();
        }, config.CHECKTIME);
    } catch (e) {
		console.log("Error at " + new Date());
		console.log(e.stack);
		setTimeout(function() {
            checkAvailability();
        }, config.CHECKTIME);
    }
}

exports.checkAvailability = checkAvailability;
exports.checklasttaken = checklasttaken;
exports.getLastCheck = getLastCheck;