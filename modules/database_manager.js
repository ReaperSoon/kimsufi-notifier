/*

-----------------------
|        server       |
-----------------------
| id (int)            |----+
| reference (varchar) |    |
| name (varchar)      |    |
-----------------------    |
                           |
                  <fk_subscriber_server>
                           |
-----------------------    |
|      subscriber     |    |
-----------------------    |
| id (int)            |    |
| type (varchar)      |    |
| contact (varchar)   |    |
| server_id (int)     |----+
| notified (int)      |
| last_notified (date)|
| deleted (boolean)   |
-----------------------

*/
"use strict";

var config = require('./config.js');
var models = require('../models');

var getAllSubscribers = function(callback, deleted) {
	var params = {
	  include: [{
			model: models.Server
		}]
	};
	if (typeof deleted !== 'undefined') {
		params.where = { deleted: true };
	}
	models.Subscriber.findAll(params)
	.then(function(subscribers) {
		return subscribers.map(function(subscriber) {
			var cleanSubsciber = subscriber.dataValues;
	     	cleanSubsciber.server = cleanSubsciber.Server.dataValues;
	     	return cleanSubsciber;
		});
	})
	.then(callback);
};

var getAllServers = function(callback) {
	models.Server.findAll()
	.then(function(servers) {
		return servers.map(function(server) {
			var cleanServer = server.dataValues;
	     	return cleanServer;
		});
	})
	.then(callback);
};

var getSubscribersByRef = function(ref, callback) {
	models.Subscriber.findAll({
	  where: {
	    deleted: false
	  },
	  include: [{
			model: models.Server, 
			where: {
	        	reference: ref
	      	},
		}]
	}).then(function(subscribers) {
		return subscribers.map(function(subscriber) {
			var cleanSubsciber = subscriber.dataValues;
	     	cleanSubsciber.server = cleanSubsciber.Server.dataValues;
	     	return cleanSubsciber;
		});
	}).then(callback);
};

var updateSubscriber = function(subscriber) {
	models.Subscriber.update(subscriber,
	{
	  where: {
	    id: subscriber.id
	  }
	});
};

var getServerByRef = function(ref, callback) {
	models.Server.findOne({ where: {reference: ref} })
	.then(function(server) {
		if (server) {
			return server;
		}else {
			return null;
		}
	})
	.then(callback);
};

var addSubscriberByRef = function(ref, contact, type, callback) {
	var result = {};

	/* Vérification que le serveur existe */
	models.Server.findOne({ where: {reference: ref} })
	.then(function(server) {
		if (server) {
			incSubscriberToServer(server);
			result.server = server.dataValues;
			/* Vérification que l'utilisateur n'est pas déjà abonné */
			return models.Subscriber.count({
			  where: {
			    contact: contact,
			    deleted: false
			  },
			  include: [{
					model: models.Server, 
					where: {
			        	reference: ref
			      	},
				}]
			});
		}else {
			result.error = 'La référence de serveur Kimsufi \'' + ref + '\' n\'existe pas.<br>' +
						   'Si le problème persiste merci de <a href="mailto:' + config.ADMIN_EMAIL + '">contacter l\'administrateur</a>';
		}
	})
	.then(function(count) {
		if (result.error) return ;
		if (count === 0) {
			/* Création du subscriber */
			return models.Subscriber.create({ 
				type: type, 
				contact: contact, 
				server_id: result.server.id, 
				notified: 0, 
				last_notified: 946681200
			});
		}else {
			result.error = 'Votre ' + (type === 'phone' ? 'téléphone' : type) + ' est déjà dans la liste du serveur Kimsufi ' + ref;
			console.log(contact + ' has already registered to ' + ref);
		}
	})
	.then(function(created) {
		if (result.error) return ;
		result.subscriber = created.dataValues;
		console.log('Subscriber created for ' + created.type + ' ' + created.contact);

	})
	.then(function() {
		/* Render */
		callback(result);
	});
};

var incSubscriberToServer = function(server) {
	server.updateAttributes({
    	subscribed: server.subscribed + 1
 	});
};

var incEmailSentToServer = function(server) {
	server.updateAttributes({
    	email_sent: server.email_sent + 1
 	});
};

var incSmsSentToServer = function(server) {
	server.updateAttributes({
    	sms_sent: server.sms_sent + 1
 	});
};

var removeSubscriberByRef = function(ref, contact, callback) {
	var result = {};

	/* Vérification que le serveur existe */
	models.Server.findOne({ where: {reference: ref} })
	.then(function(server) {
		if (server) {
			/* Récupération des subscribers abonnés à ce serveur avec ce téléphone/email */
			result.server = server.dataValues;
			return models.Subscriber.findAll({
			  where: {
			    contact: contact,
			    deleted: false
			  },
			  include: [{
					model: models.Server, 
					where: {
			        	reference: ref
			      	},
				}]
			});
		}else {
			result.error = 'La référence de serveur Kimsufi \'' + ref + '\' n\'existe pas.<br>' +
						   'Si le problème persiste merci de <a href="mailto:' + config.ADMIN_EMAIL + '">contacter l\'administrateur</a>';
		}
	})
	.then(function(subscribers) {
		if (result.error) return ;

		var cleanSubscibers = [];
		for (var i = subscribers.length - 1; i >= 0; i--) {
			var subscriber = subscribers[i];
			/* Foreach subscriber unsubscribe from server */
			subscriber.updateAttributes({
		    	deleted: true
	     	});
	     	var cleanSubsciber = subscriber.dataValues;
	     	cleanSubsciber.server = cleanSubsciber.Server.dataValues;
	     	cleanSubscibers.push(cleanSubsciber);
		}
		if (subscribers.length > 0) {
			result.subscribers = cleanSubscibers;
		}else {
			result.error = 'Vous n\'êtes actuellement pas abonnés aux notifications du serveur Kimsufi ' + result.server.name + '.<br>' +
						   'Si le problème persiste merci de <a href="mailto:' + config.ADMIN_EMAIL + '">contacter l\'administrateur</a>';
		}
	}).then(function() {
		/* Render */
		if (typeof callback !== 'undefined') {
			callback(result);
		}
	});
};

var removeSubscriberFromAll = function(contact, callback) {
	var result = {};

			/* Récupération des subscribers abonnés à ce serveur avec ce téléphone/email */
	models.Subscriber.findAll({
		where: {
			contact: contact,
			deleted: false
		},
		include: [{
			model: models.Server, 
		}]
	})
	.then(function(subscribers) {
		console.log(subscribers.length + ' subscribers to be updated');
		var cleanSubscibers = [];
		for (var i = subscribers.length - 1; i >= 0; i--) {
			var subscriber = subscribers[i];
			/* Foreach subscriber unsubscribe from server */
			subscriber.updateAttributes({
		    	deleted: true
	     	});
	     	var cleanSubsciber = subscriber.dataValues;
	     	cleanSubsciber.server = cleanSubsciber.Server.dataValues;
	     	cleanSubscibers.push(cleanSubsciber);
		}
		if (subscribers.length > 0) {
			result.subscribers = cleanSubscibers;
		}else {
			result.error = 'Vous n\'êtes abonnés à aucune notification de serveur Kimsufi sur ce site.<br>' +
						   'Si le problème persiste merci de <a href="mailto:' + config.ADMIN_EMAIL + '">contacter l\'administrateur</a>';
		}
	}).then(function() {
		/* Render */
		callback(result);
	});
};

exports.getAllServers = getAllServers;
exports.updateSubscriber = updateSubscriber;
exports.getSubscribersByRef = getSubscribersByRef;
exports.addSubscriberByRef = addSubscriberByRef;
exports.removeSubscriberByRef = removeSubscriberByRef;
exports.removeSubscriberFromAll = removeSubscriberFromAll;
exports.getAllSubscribers = getAllSubscribers;
exports.getServerByRef = getServerByRef;
exports.incSubscriberToServer =incSubscriberToServer;
exports.incEmailSentToServer = incEmailSentToServer;
exports.incSmsSentToServer = incSmsSentToServer;