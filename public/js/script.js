var validateEmail = function(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};

var validatePhone = function(phone) {
    return ((phone.indexOf('06') === 0 || phone.indexOf('07') === 0) && phone.length === 10);
};

var checkSubscribeForm = function() {
	if ((validateEmail($('input[name="email"]').val()) || validatePhone($('input[name="phone"]').val())) && $("select[name='server'] option:selected").val() !== '') {
		$('input[type="submit"]').prop('disabled', false);
		return true;
	}
	$('input[type="submit"]').prop('disabled', true);
	return false;
}

var checkUnsubscribeForm = function() {
	if (validateEmail($('input[name="email"]').val()) || validatePhone($('input[name="phone"]').val())) {
		$('input[type="submit"]').prop('disabled', false);
		return true;
	}
	$('input[type="submit"]').prop('disabled', true);
	return false;
}